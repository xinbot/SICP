(define (even? x) (= (remainder x 2) 0))

(define (odd? x) (= (remainder x 2) 1))

(define (same-parity a . b)
  (define (same-parity-iter f list)
    (cond ((null? list) ())
	  ((f (car list)) (cons (car list) (same-parity-iter f (cdr list))))
	  (else
	   (same-parity-iter f (cdr list)))))
  (if (even? a)
      (same-parity-iter even? (cons a b))
      (same-parity-iter odd? (cons a b))))

(same-parity 1 2 3 4 5 6 7)

(same-parity 2 3 4 5 6 7)