(define (make-interval a b) (cons a b))

(define (lower-bound i) (car i))

(define (upper-bound i) (cdr i))

(define (make-center-percent c p)
  (let ((width (* c p)))
    (make-interval (- c width) (+ c width))))

(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))

(define (percent i)
  (let ((width (/ (- (upper-bound i) (lower-bound i)) 2.0))
	(center (/ (+ (upper-bound i) (lower-bound i)) 2.0)))
    (/ width center)))

