;;a
(define (left-branch mobile)
  (car mobile))

(define (right-branch mobile)
  (car (cdr mobile)))

(define (branch-length branch)
  (car branch))

(define (branch-structure branch)
  (car (cdr branch)))

;;b
(define (total-weight mobile)
  (+ (branch-weight (left-branch mobile))
     (branch-weight (right-branch mobile))))

(define (branch-weight branch)
  (let ((structure (branch-structure branch)))
    (if (pair? structure)
	(total-weight structure)
	structure)))

;;c
(define (torque branch)
  (* (branch-length branch) (branch-weight branch)))
 
(define (balanced? mobile)
  (let ((left (left-branch mobile))
        (right (right-branch mobile)))
    (and (= (torque left) (torque right))
         (if (pair? (branch-structure left)) (balanced? left) #t)
         (if (pair? (branch-structure right)) (balanced? right) #t))))


;;d
(define (left-branch mobile) (car mobile))
 
(define (right-branch mobile) (cdr mobile))
 
(define (branch-length branch) (car branch))
 
(define (branch-structure branch) (cdr branch))
