; When encode a most frequent symbol, only one branch need to be searched to find the most frequent
; symbol, the order of growth is O(1)
; When encode a least frequent symbol, both the left and right branches needed to be searched at 
; every node, the order of growth is O(n) + O(n-1) + O(n-2) + ... + O(1), which is O(n^2).
;