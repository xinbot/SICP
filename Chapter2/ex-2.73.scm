; a. The above 'deriv' function apply the data-directed method. The number? use eq? as the
;    underlaying comparing method while same-variable? use equal?. These two are different method
;    of comparison so we can not assimilate the data-directed dispatch.

; b.
(define (install-sum-package)
  ;; internal procedures
  (define (make-sum a1 a2) (cons a1 a2))
  (deifne (addend s) (cadr s))
  (define (augend s) (caddr s))
  (define (deriv-sum s)
    (make-sum (deriv (addend s)) (deriv (augend s))))
  
  ;; interface to the rest of the system
  (define (tag x) (attach-tag '+ x))
  (put 'deriv '(+) deriv-sum)
  (put 'make-sum '+
       (lambda (x y) (tag (make-sum x y))))
  'done)

(define (install-product-package)
  ;; internal procedures
  (define (make-product m1 m2) (cons m1 m2))
  (define (multiplier p) (cadr p))
  (define (multiplicand p) (caddr p))
  (define (deriv-product p)
    (make-sum
     (make-product (multiplier exp)
		   (deriv (multiplicand exp) var))
     (make-product (deriv (multiplier exp) var)
		   (multiplicand exp))))

  ;; interface to the rest of the system
  (define (tag x) (attach-tag '* x))
  (put 'deriv '(*) deriv-product)
  (put 'make-product '* 
       (lambda (x y) (tag (make-product x y))))
  'done)

(define (make-sum x y) ((get 'make-sum '+) x y))

(define (make-product x y) ((get 'mkae-product '*) x y))

(define (deriv x) (apply-generic 'deriv x))

; c.
(define (make-exponentiation base exponent)
  (cond ((=number? exponent 0) 1)
	((=number? exponent 1) base)
	((=number? base 1) 1)
	(else (list '** base exponent))))
(define (exponent expr) (cadr expr))
(define (base expr) (car expr))
(define (exponentation-deriv expr var)
  (make-product (exponent expr)
		(make-product
		 (make-exponentiation (base expr)
				      (make-sum (exponent expr) -1))
		 (deriv (base expr) var))))
(put 'deriv '** exponentiation-deriv)

; d. We need to change the order of arguments in procedure "put".


