(car ''abracadabra)

;;This expression can be written as (car (quote (quote abracadabra))) or (car '(quote abracadabra))
;;This gives us the symbol quote