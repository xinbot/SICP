(define (square x) (* x x))

(define (append list1 list2)
  (if (null? list1)
      list2
      (cons (car list1) (append (cdr list1) list2))))

(define (square-list items)
  (define (iter things answer)
    (if (null? things)
	answer
	(iter (cdr things)
	      (append answer (list (square (car things)))
		    ))))
  (iter items nil))

;;a
;;The arugment list is traversed in order. The squares are successively added to the front of 
;;the answer via cons

;;b
;;To build a list of integers requires a sequence of pairs created by nested conses, where the first
;;argument is an integer and the second is a list created by cons (or the empty list) 

(square-list (list 1 2 3 4 5 6 7))






















