(define nil ())

(define (append list1 list2)
  (if (null? list1)
      list2
      (cons (car list1) (append (cdr list1) list2))))

(define (reverse-recursive list)
  (if (or (null? list) (null? (cdr list)))
      list
      (append (reverse-recursive (cdr list)) (cons (car list) nil))))

(define (reverse-iterative list)
  (define (reverse-iter list result)
    (if (null? list)
	result
	(reverse-iter (cdr list) (cons (car list) result))))
  (reverse-iter list nil))

(reverse-iterative (list 1 4 9 16 25))
