;;Assume Ca is center of a, Ta is tolerance of a
;;       Cb is center of b, Tb is tolerance of b
;;
;;a = [Ca*(1 - 0.5*Ta), Ca*(1 + 0.5*Ta)]
;;b = [Cb*(1 - 0.5*Tb), Cb*(1 + 0.5*Tb)]
;;
;;a*b =  [Ca*Cb*(1 - 0.5*(Ta + Tb) + 0.25*Ta*Tb),
;;        Ca*Cb*(1 + 0.5*(Ta + Tb) + 0.25*Ta*Tb)]
;;Ta*Tb will be a wee number, so it can be ignored. The tolerance of the product will
;;be the approximately the sum of the component tolerances.

