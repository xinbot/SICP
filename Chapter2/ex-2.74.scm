; a. Each division's records consist of a single file, which contains a set of records keyed on 
;    employees' names. This single file should be structured as a list of employees' records, with 
;    a tag of the division name. And each employee;s record should have a tag of their name.
(define (install-division-package)
  ;; internal procedures
  (define (get-record name file)
    (cond ((null? file) (error "no result"))
	  ((eq? name (get-name (cadr file))) (cons (cadr file)
						   (get-record name (cdr file))))
	  (else (get-record name (cdr file)))))
  (define (get-name record) (car record))

  ;; interface to the rest of the system
  (put 'get-record 'division get-record)
  (put 'get-name 'division get-name)
  'done)

(define (get-record name file)
  (apply-generic 'get-record name file))

(define (apply-generic op name file)
  (let ((division-name (type-tag file)))
    (let ((proc (get op division-name)))
      (if proc
	  (proc name file)
	  (error "no result")))))

(define (type-tag file) (car file))

; b.
(define (get-salary name file)
  (cond ((null? file) (error "no result"))
	((eq? name (get-name (cadr file))) (cons (cadr (cadr file))
						 (get-salary name (cdr file))))
	(else (get-salary name (cdr file)))))

(put 'get-salary 'division get-salary)

;; Addition to the environment
(define (get-salary name file)
  (apply-generic 'get-salary name file))

; c.
(define (find-employee-record name list)
  (if (null? list)
      (error "no result")
      (append (get-record (car list))
	      (find-employee-record name (cdr list)))))

; d. Install a new package which specifies the procedures to look up name / salary in the new company's file.
