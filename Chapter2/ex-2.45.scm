(define (split identity-op smaller-op)
  (define (rec-split painter n)
    (if (= n 0)
	painter
	(let ((smaller (rec-split painter (- n 1))))
	  (identity-op painter (smaller-op smaller smaller)))))
  rec-split)
  