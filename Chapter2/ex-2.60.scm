;The number of steps required by procedure adjoin-set has gone from O(n) to O(1)

;The orders of growth are the same for intersection-set and union-set

;Storage for sets will require at least as much space in this representation as they did in the
;"no duplicates" representation.

;We would probably use "duplicates" representation for applications in which adjoin-set is a
;frequent operation.