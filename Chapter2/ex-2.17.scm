(define (last-pair-recursive list)
  (if (or (null? list) (null? (cdr list)))
      list
      (last-pair-recursive (cdr list))))

(define (last-pair-iterative list)
  (define (last-pair-iter list result)
    (if (null? list)
	result
	(last-pair-iter (cdr list) list)))
  (last-pair-iter list list))
