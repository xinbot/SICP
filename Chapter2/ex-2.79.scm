(define (install-scheme-number-package)
  ;; ......
  (put 'equ? '(scheme-number scheme-number) =)
  'done)

(define (install-rational-package)
  ;; ......
  (define (equ? x y)
    (= (* (numer x) (denom y)) (* (numer y) (denom x))))
  ;; ......
  (put 'equ? '(rational rational) equ?)
  'done)

(define (install-rectanglar-package)
  ;; ......
  (put 'equ? '(rectanglar rectanglar)
       (lambda (x y) (and (= (real-part x) (real-part y))
			  (= (imag-part x) (imag-part y)))))
  'done)

(define (install-polar-package)
  ;; .......
  (put 'equ? '(polar polar)
       (lambda (x y) (and (= (magnitude x) (magnitude y))
			  (= (angle x) (angle y)))))
  'done)

(define (install-complex-packages)
  ;; ......
  (put 'equ? '(complex complex) equ?)
  'done)

(define (equ? x y) (apply-generic 'equ? x y))
