(define x (list (list 1 2) (list 3 4)))

(define (append list1 list2)
  (if (null? list1)
      list2
      (cons (car list1) (append (cdr list1) list2))))

(define (reverse list)
  (if (or (null? list) (null? (cdr list)))
      list
      (append (reverse (cdr list)) (cons (car list) ()))))

(define (deep-reverse list)
  (cond ((null? list) ())
        ((pair? list)
         (append (deep-reverse (cdr list)) (cons (deep-reverse (car list)) ())))
        (else list)))

(deep-reverse x)
