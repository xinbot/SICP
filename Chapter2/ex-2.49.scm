(define nil ())

(define one 0.99)

(define (make-vect x y) (cons x y))

(define origin (make-vect 0 0))

(define lower-right (make-vect one 0))

(define upper-left (make-vect 0 one))

(define upper-right (make-vect one one))

;;a
(define (outline frame)
  ((segments->painter (list (make-segment origin lower-right)
			    (make-segment lower-right upper-right)
			    (make-segment upper-right upper-left)
			    (make-segment upper-left origin)))
   frame))

;;b
(define (x-marks-the-spot frame)
  ((segments->painter (list (make-segment origin upper-right)
			    (make-segment lower-right upper-left)))
   frame))

;;c
(define (diamond frame)
    ((segments->painter (list (make-segment (make-vect 0.5 0) (make-vect one 0.5))
			      (make-segment (make-vect one 0.5) (make-vect 0.5 one))
			      (make-segment (make-vect 0.5 one) (make-vect 0 0.5))
			      (make-segment (make-vect 0 0.5) (make-vect 0.5 0))))
     frame))

;;d
(define wave 
   (segments->painter (list 
                       (make-segment (make-vect .25 0) (make-vect .35 .5)) 
                       (make-segment (make-vect .35 .5) (make-vect .3 .6)) 
                       (make-segment (make-vect .3 .6) (make-vect .15 .4)) 
                       (make-segment (make-vect .15 .4) (make-vect 0 .65)) 
                       (make-segment (make-vect 0 .65) (make-vect 0 .85)) 
                       (make-segment (make-vect 0 .85) (make-vect .15 .6)) 
                       (make-segment (make-vect .15 .6) (make-vect .3 .65)) 
                       (make-segment (make-vect .3 .65) (make-vect .4 .65)) 
                       (make-segment (make-vect .4 .65) (make-vect .35 .85)) 
                       (make-segment (make-vect .35 .85) (make-vect .4 1)) 
                       (make-segment (make-vect .4 1) (make-vect .6 1)) 
                       (make-segment (make-vect .6 1) (make-vect .65 .85)) 
                       (make-segment (make-vect .65 .85) (make-vect .6 .65)) 
                       (make-segment (make-vect .6 .65) (make-vect .75 .65)) 
                       (make-segment (make-vect .75 .65) (make-vect 1 .35)) 
                       (make-segment (make-vect 1 .35) (make-vect 1 .15)) 
                       (make-segment (make-vect 1 .15) (make-vect .6 .45)) 
                       (make-segment (make-vect .6 .45) (make-vect .75 0)) 
                       (make-segment (make-vect .75 0) (make-vect .6 0)) 
                       (make-segment (make-vect .6 0) (make-vect .5 .3)) 
                       (make-segment (make-vect .5 .3) (make-vect .4 0)) 
                       (make-segment (make-vect .4 0) (make-vect .25 0)) 
                       ))) 

