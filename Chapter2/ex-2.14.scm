(define (make-interval a b) (cons a b))
 
(define (lower-bound i) (car i))
 
(define (upper-bound i) (cdr i))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))

(define (sub-interval x y)
  (add-interval x (make-interval (- (upper-bound y)) (- (lower-bound y)))))
 
(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))
 
(define (div-interval x y)
  (if (= 0 (* (upper-bound y) (lower-bound y))) 
      (error "Interval spans 0" y)
      (mul-interval x
		    (make-interval (/ 1.0 (upper-bound y))
				    (/ 1.0 (lower-bound y))))))

(define (interval-width i)
  (/ (- (upper-bound i) (lower-bound i)) 2.0))

(define (make-center-percent c p)
  (let ((width (* c p)))
    (make-interval (- c width) (+ c width))))

(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2.0))

(define (percent i)
  (let ((width (/ (- (upper-bound i) (lower-bound i)) 2.0))
	(center (/ (+ (upper-bound i) (lower-bound i)) 2.0)))
    (/ width center)))

(define (par1 r1 r2)
  (div-interval (mul-interval r1 r2)
		(add-interval r1 r2)))

(define (par2 r1 r2)
  (let ((one (make-interval 1 1)))
    (div-interval one
		  (add-interval (div-interval one r1)
				(div-interval one r2)))))

(define A (make-center-percent 5 .012))
(define B (make-center-percent 9 .003))

;;The centers are the same
(center (par1 A B))

(center (par2 A B))

;;The tolerances are different
(percent (par1 A B))

(percent (par2 A B))



