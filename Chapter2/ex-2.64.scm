; Part A
; partial-tree divides its input list into 3 parts: 
;           1. a new list containing the first (n - 1) / 2 elements
;           2. the middle element 
;           3. a new list containing the remaining element
;
; The two new list are created recursively by applying partial-tree to each half of the input list.
; If the list is empty, partial-tree returns an empty list, which corresponds to the end of a branch. It then creates a balanced tree rotted at the middle element whose left branch is the first new list and whose right branch is the second new list.
; 
;           5
;          / \
;         3   9
;        /   / \
;       1   7   11
;
; Part B
; At each step, partial-tree splits a list of length n into two lists of approximate length n / 2.
; The work done to split the list is (quotient (- n 1) 2) and (- n (+ left-size 1)), both of which 
; take constant time. Therefore, the time to make the partial tree of a list of n elements is:
; T(n) = 2T(n / 2) + O(1) ==> O(n)
;
; The time taken by list->tree for a list of length n will be the time taken by partial-tree plus
; the time taken by length for that list. So the order of growth of list->tree is O(n)
;
