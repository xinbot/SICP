;; scheme-number-package
(put '=zero? 'scheme-number (lambda (x) (= x 0)))

;; rational-number-package
(put '=zero? 'rational-number (lambda (x) (= (number x) 0)))

;; complex-number-package
(put '=zero? 'complex-number (lambda (x) (= (real-part x) (imag-part x) 0)))

(define (=zero? x) (apply-generic '=zero? x))