; For n = 5
;         *
;        / \
;       *   16
;      / \
;     *   8
;    / \
;   *   4
;  / \
; 1   2
;
; For n = 10
;                   *
;                  / \
;                 *   512
;                / \
;               *   256
;              / \
;             *   128
;            / \
;           *   64 
;          / \
;         *   32
;        / \
;       *   16
;      / \
;     *   8
;    / \
;   *   4
;  / \
; 1   2
;
; The most frequent symbol needed 1 bit to encode. 
; The least frequent symbol needed n - 1 bits to encode.
;


