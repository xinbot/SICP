(define (even? n)
  (= (remainder n 2) 0))

(define (expmod base exp m)
  (remainder (fast-expt base exp) m))

(define (fast-expt b n)
  (cond ((= n 0) 1)
	((even? n) (square (fast-expt b (/ n 2))))
	(else (* b (fast-expt b (- n 1))))))

(define (square x) (* x x))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
	((fermat-test n) (fast-prime? n (- times 1)))
	(else false)))

(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (fast-prime? n 100)
      (report-prime (- (runtime) start-time))
      #f))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (search-for-prime n counter)
  (if (even? n)
      (search-for-prime-iter (+ n 1) counter)
      (search-for-prime-iter n counter)))

(define (search-for-prime-iter n counter)
  (if (> counter 0)
      (if (timed-prime-test n)
	  (search-for-prime-iter (+ n 2) (- counter 1))
	  (search-for-prime-iter (+ n 2) counter))
      #t))

;;The modified version of expmod takes considerably longer time to compute the same resuts
;;The remainder operation inside the original expmod implementation, keeps the numbers being
;;squared less than the number tested for primality m. fast-expt however squares huge number
;; of a^m size

