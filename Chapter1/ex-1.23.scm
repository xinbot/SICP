(define (smallest-divisor n)
  (find-divisor n 2))

(define (even? n)
  (= (remainder n 2) 0))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
	((divides? test-divisor n) test-divisor)
	(else (find-divisor n (next test-divisor)))))

(define (next test-divisor)
  (if (= test-divisor 2) 
      3
      (+ test-divisor 2)))

(define (divides? a b)
  (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))
      #f))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (search-for-prime n counter)
  (if (even? n)
      (search-for-prime-iter (+ n 1) counter)
      (search-for-prime-iter n counter)))

(define (search-for-prime-iter n counter)
  (if (> counter 0)
      (if (timed-prime-test n)
	  (search-for-prime-iter (+ n 2) (- counter 1))
	  (search-for-prime-iter (+ n 2) counter))
      #t))
