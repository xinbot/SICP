;;Rewritten expmod generates a tree recursion, whose execution time grows exponentially with the
;;depth of the tree, which is logarithm of N. O(log2^n)
