(define (cont-frac n d k)
  (define (cont-frac-iter counter result)
    (if (= counter 0)
	 result
	(cont-frac-iter (- counter 1)  (/ (n counter) (+ (d counter) result)))))
  (cont-frac-iter k 0.0))

(define (tan-cf x k)
  (cont-frac (lambda (i) 
	       (if (= i 1)
		   x
		   (- (* x x))))
	     (lambda (i) 
	       (- (* 2 i) 1))
	     k))




