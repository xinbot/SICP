(define (inc n) (+ n 1))

(define (even? n) (= (remainder n 2) 0))

(define (cube n) (* n n n))

(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a) 
	 (sum term (next a) next b))))

(define (simpson f a b n)
  (define h (/ (- b a) n))
    (define (term k)
      (define y (f (+ a (* k h))))
      (if (or (= k 0) (= k n))
	  (* 1 y)
	  (if (even? k)
	      (* 2 y)
	      (* 4 y))))
    (* (/ h 3) (sum term 0 inc n)))

(simpson cube 0 1.0 100)

(simpson cube 0 1.0 1000)





