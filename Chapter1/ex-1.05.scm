;;Result of using applicative-order interpreter:
;;(test 0 (p)) the second operand is an infinite loop so the expression never produce an answer.

;;Result of using normal-order interpreter:
;;(test 0 (p))
;;(if (= x 0) 0 y)
;;(if #t 0 y)
;;(0)









































