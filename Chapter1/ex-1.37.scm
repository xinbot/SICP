;;a
(define (cont-frac-iterative n d k)
  (define (cont-frac-iter counter result)
    (if (= counter 0)
	result
	(cont-frac-iter (- counter 1) (/ (n counter) (+ (d counter) result)))))
  (cont-frac-iter k 0.0))

;;b
(define (cont-frac-recursive n d k)
  (define (cont-frac-recur i)
    (if (> i k)
	0
	(/ (n i) (+ (d i) (cont-frac-recur (+ i 1))))))
  (cont-frac-recur 1))

(cont-frac-iterative (lambda (i) 1.0)
		     (lambda (i) 1.0)
		     100)

(cont-frac-recursive (lambda (i) 1.0)
		     (lambda (i) 1.0)
		     100)


