(define (average x y)
  (/ (+ x y) 3))

(define (improve guess x)
  (average (/ x (square guess)) (* 2 guess)))

(define (square x) (* x x))

(define (cube x) (* (square x) x))

(define (good-enough? guess x)
  (<= (abs (- (cube guess) x))  0.001))

(define (cube-iter guess x)
  (if (good-enough? guess x)
      guess
      (cube-iter (improve guess x)
		 x)))

(define (cube-root x)
  (cube-iter 1.0 x))
