(define (square x) (* x x))

(define (even? n)
  (= (remainder n 2) 0))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
	((even? exp)
	 (if (non-trivial-sqrt? (expmod base (/ exp 2) m) m)
	     0
	     (remainder (square (expmod base (/ exp 2) m)) 
			m)))
	(else
	 (remainder (* base (expmod base (- exp 1) m))
		    m))))

(define (non-trivial-sqrt? a n)
  (cond ((= a 1) 0)
	((= a (- n 1)) 0)
	(else (= (remainder (square a) n) 1))))

(define (miller-rabin-test a n)
  (cond ((= a 0) #t)
	((= (expmod a (- n 1) n) 1) (miller-rabin-test (- a 1) n))
	(else #f)))

(define (prime? n)
  (miller-rabin-test (- n 1) n))

