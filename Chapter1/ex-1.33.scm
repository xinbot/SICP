(define (filtered-accumulate combiner null-value term a next b filter)
  (if (> a b)
      null-value
      (if (filter a)
	  (combiner (term a) (filtered-accumulate combiner null-value term (next a) next b filter))
	  (combiner null-value (filtered-accumulate combiner null-value term (next a) next b filter)))))

(define (square n) (* n n))

(define (inc n) (+ n 1))

;;a
(define (smallest-div n)
  (define (divides? a b)
    (= 0 (remainder b a)))
  (define (find-div n test)
    (cond ((> (square test) n) n) ((divides? test n) test)
	  (else (find-div n (+ test 1)))))
  (find-div n 2))

(define (prime? n)
  (if (= n 1) #f (= n (smallest-div n))))

(define (sum-of-prime-squares a b)
  (filtered-accumulate + 0 square a inc b prime?))

;;b
(define (gcd m n)
  (cond ((< m n) (gcd n m))
	((= n 0) m)
	(else (gcd n (remainder m n)))))

(define (relative-prime? m n)
  (= (gcd m n) 1))

(define (product-of-relative-primes n)
  (filtered-accumulate * 1 identity 1 inc n relative-prime?))