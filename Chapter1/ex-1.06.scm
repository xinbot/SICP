(define (average x y)
  (/ (+ x y) 2))

(define (improve guess x)
  (average guess (/ x guess)))

(define (square x) (* x x))

(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 0.001))

(define (sqrt-iter guess x)
  (new-if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x)
		 x)))

(define (sqrt x)
  (sqrt-iter 1.0 x))

(define (new-if predicate then-clause else-clause)
  (cond (predicate then-clause)
	(else else-clause)))

(sqrt 9)

;;Suggest Reading: http://mitpress.mit.edu/sicp/book/node85.html
;;When replace if by new-if the square root procedure enters into an infinite recursion loop.
;;new-if is a procedure, not a special-form. That is sub-expressions are evaluated before new-if is applied to the values of the operands.
