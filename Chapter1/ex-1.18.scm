(define (double x) (* x 2))

(define (halve x) (/ x 2))

(define (even? x) (= (remainder x 2) 0))

(define (fast-mul a b)
  (fast-mul-iter a b 0))

(define (fast-mul-iter a b result)
  (cond ((= b 0) result)
	((even? b) (fast-mul-iter (double a) (halve b) result))
	(else (fast-mul-iter a (- b 1) (+ a result)))))
