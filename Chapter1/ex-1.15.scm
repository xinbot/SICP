;;a. (sine 12.15)
;;   (p (sine 4.05))
;;   (p (p (sine 1.35)))
;;   (p (p (p (sine 0.45))))
;;   (p (p (p (p (sine 0.15)))))
;;   (p (p (p (p (p (sine 0.05))))))
;;   (p (p (p (p (p 0.05)))))
;;
;;   The procedure p is applied five times.
;;
;;b. The procedure p is applied once for each complete power of 3 contained within the angle a. 
;;   (ceiling (/ (log (/ a 0.1)) (log 3)))
;;   In other words that is O(log(x)) order of growth. 



