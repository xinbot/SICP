(define (cont-frac n d k)
  (define (cont-frac-iter counter result)
    (if (= counter 0)
	result
	(cont-frac-iter (- counter 1) (/ (n counter) (+ (d counter) result)))))
  (cont-frac-iter k 0.0))

(Define (e-euler n)
  (+ 2.0 (cont-frac (lambda (i) 1.0)
		    (lambda (i)
		      (if (= (remainder i 3) 2)
			  (/ (+ i 1) 1.5)
			  1))
		    n)))



