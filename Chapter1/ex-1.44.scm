(define dx 0.00001)

(define (average a b c) (/ (+ a b c) 3))

(define (compose f g)
  (lambda (x)
    (f (g x))))

(define (repeated f n)
  (lambda (x)
    (if (= n 0)
	x
	((compose (repeated f (- n 1)) f) x))))

(define (smooth f)
  (lambda (x)
    (average (f (- x dx)) (f x) (f (+ x dx)))))

(define (n-fold-smooth f n)
  ((repeated smooth n) f))
