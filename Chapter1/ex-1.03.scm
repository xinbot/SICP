(define (square x) (* x x))

(define (sum-of-squares x y) (+ (square x) (square y)))

(define (larger x y) (if (> x y) x y))

(define (sum-of-two-larger-squares x y z)
  (cond ((= (min x y z) x) (sum-of-squares y z))
	((= (min x y z) y) (sum-of-squares x z))
	((= (min x y z) z) (sum-of-squares x y))))



	