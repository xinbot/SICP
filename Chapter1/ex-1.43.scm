(define (compose f g)
  (lambda (x)
    (f (g x))))

(define (repeated f n)
  (lambda (x)
    (if (= n 0)
	x
	((compose (repeated f (- n 1)) f) x))))

(define (square x) (* x x))

((repeated square 2) 5)