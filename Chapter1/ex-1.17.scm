(define (double x) (* x 2))

(define (halve x) (/ x 2))

(define (even? x) (= (remainder x 2) 0))

(define (fast-mul a b)
  (cond ((= b 0) 0)
	((even? b) (double (fast-mul-recursive a (halve b))))
	(else (+ a (fast-mul-recursive a (- b 1))))))



