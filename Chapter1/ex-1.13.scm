;;Suppose there exist two numbers that would fit the equation
;;Fib(n) = (a^n - b^n) / (sqrt 5), (n >= 0). It would be true whatever n's value is. 
;;So when n equals 1 and 2:
;;
;;1 = (a - b) / (sqrt 5)      so: a - b = (sqrt 5)   so: a = (1 + (sqrt 5)) / 2
;;1 = (a^2 - b^2) / (sqrt 5)      a + b = 1              b = (1 - (sqrt 5)) / 2
;;
;;We already know that this set of values fits the equation when n = 0 & n = 1,
;;so we only need to prove that it remains true for all n >= 2. which means 
;;a^n - b^n = a^(n - 1) + a^(n - 2) should be true for n >= 2,
;;which means for n >= 0, there should be:
;;
;;(a^n+2 - b^n+2) / (sqrt 5) = ((a^n+1 - b^n+1) / (sqrt 5)) - ((a^n - b^n) / (sqrt 5))
;;
;;From this we could get:
;;
;;a^n+2 - a^n+1 - a^n = b^n+2 - b^n+1 - b^n
;;and a * (a^2 - a - 1) = b^n * (b^2 - b - 1)
;;
;;Since for a = (1 + (sqrt 5)) / 2  there exist a^2 - a - 1 = 0
;;          b = (1 - (sqrt 5)) / 2              b^2 - b - 1 = 0
;;
;;Thus the equation is true for n >= 2
;;Thus Fib(n) = (a^n - b^n) / (sqrt 5), n>=0
;;
;;So Fib(n) = (a^n / (sqrt 5)) - (b^n / (sqrt 5))
;;          = (a^n / (sqrt 5)) + (((sqrt 5) - 1) / 2)^n * (1 / (sqrt 5))
;;
;;Because: 
;;         2 < (sqrt 5) < 3
;;         1 < (sqrt 5) - 1 < 2
;;         1 / 2 < ((sqrt 5) - 1) / 2 < 1
;;
;;So:
;;         1 / 4 < (((sqrt 5) - 1) / 2)^n < 1 (while n >= 2)
;;         1 / 12 < (((sqrt 5) - 1) / 2)^n / (sqrt 5) < 1 / 2
;;
;;Thus Fib(n) is the closest integer to (((sqrt 5) + 1) / 2)^n / (sqrt 5)

