(define (average x y)
  (/ (+ x y) 2))

(define (improve guess x)
  (average guess (/ x guess)))

(define (square x) (* x x))

(define (good-enough? guess oldguess)
  (<= (abs (- guess oldguess)) (* 0.0001 guess)))

(define (sqrt-iter guess oldguess x)
  (if (good-enough? guess oldguess)
      guess
      (sqrt-iter (improve guess x)
		 guess x)))

(define (sqrt x)
  (sqrt-iter 1.0 0.0 x))
