;;a
(define (product term a next b)
  (if (> a b)
      1
      (* (term a) (product term (next a) next b))))

(define (identity n) n)

(define (inc n) (+ n 1))

(define (even? n) (= (remainder n 2) 0))

(define (factorial n)
  (product identity 1 inc n))

(define (pi-product a b)
  (define (pi-term n)
    (if (even? n)
	(/ (+ n 2) (+ n 1))
	(/ (+ n 1) (+ n 2))))
  (product pi-term a inc b))

;;b
(define (product-iter term a next b)
  (define (iter a result)
    (if (> a b)
	result
	(iter (next a) (* (term a) result)))
    (iter a 1)))
