(define (iterative-improve improve good-enough?)
  (lambda (x)
    (let ((next-guess (improve x)))
      (if (good-enough? next-guess x)
	  next-guess
	((iterative-improve improve good-enough?) next-guess)))))

(define (square x) (* x x))

(define (average a b) (/ (+ a b) 2))

(define (sqrt x)
  (define (good-enough? next-guess guess)
    (< (abs (- (square guess) x)) 0.001))
  (define (improve guess)
    (average guess (/ x guess)))
  ((iterative-improve improve good-enough?) 1.0))

(define tolerance 0.00001)

(define (fixed-point f first-guess)
  (define (good-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  ((iterative-improve f good-enough?) first-guess))

(define (average-damp f)
  (lambda (x) (average x (f x))))

(define (fixed-point-sqrt x)
  (fixed-point (average-damp (lambda (y) (/ x y)))
	       1.0))
