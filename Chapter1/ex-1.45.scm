(define tolerance 0.00001)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
	  next
	  (try next))))
  (try first-guess))

(define (compose f g)
  (lambda (x)
    (f (g x))))

(define (repeated f n)
  (lambda (x)
    (if (= n 0)
	x
	((compose (repeated f (- n 1)) f) x))))

(define (average a b) (/ (+ a b) 2))

(define (average-damp f)
  (lambda (x) (average x (f x))))

(define (log2 x) (/ (log x) (log 2)))

(define (square x) (* x x))

(define (pow b p) 
   (define (even? x) 
     (= (remainder x 2) 0)) 
   (define (square x) 
     (* x x)) 
   (define (iter res a n) 
     (if (= n 0) 
         res 
         (if (even? n) 
             (iter res (square a) (/ n 2)) 
             (iter (* res a) a (- n 1))))) 
   (iter 1 b p)) 

(define (nth-root n x)
  (fixed-point ((repeated average-damp (floor (log2 n)))
		(lambda (y) (/ x (pow y (- n 1)))))
		1.0))
